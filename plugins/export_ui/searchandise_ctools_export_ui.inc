<?php

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  // As defined in hook_schema().
  'schema' => 'searchandise_keywords',
  // Define a permission users must have to access these pages.
  'access' => 'administer searchandise',

  // Define the menu item.
  'menu' => array(
    'menu item' => 'searchandise',
    'menu prefix' => 'admin/config/search',
    'menu title' => 'Searchandise Settings',
    'menu description' => 'Add and remove actions for searches.',
  ),

  // Define user interface texts.
  'title singular' => t('keyword'),
  'title plural' => t('keywords'),
  'title singular proper' => t('Searchandise keyword'),
  'title plural proper' => t('Searchandise keywords'),
    
  'handler' => array(
    'class' => 'searchandise_keywords_ui',
    'parent' => 'ctools_export_ui',
  ),

  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'searchandise_ctools_export_ui_form',
    'validate' => 'searchandise_ctools_export_ui_form_validate',
    // 'submit' and 'validate' are also valid callbacks.
  ),
);

/**
 * Implements hook_ctools_export_ui_form().
 */
function searchandise_ctools_export_ui_form(&$form, &$form_state) {
  $keyword = $form_state['item'];
  unset($form_state['input']['parameter']);
  $form['keywords'] = array(
    '#type' => 'textarea',
    '#title' => t('Keywords'),
    '#description' => t('Add keywords comma seperated'),
    '#required' => TRUE,
    '#default_value' => $keyword->keywords,
  );
  $form['action'] = array(
    '#type' => 'select',
    '#title' => t('Action'),
    '#description' => t('Add action for the keyword'),
    '#required' => TRUE,
    '#default_value' => $keyword->action,
    '#options' => _searchandise_action_list(),
    '#ajax' => array(
       'callback' => 'searchandise_form_ajax_callback',
       'wrapper' => 'searchandise_form_ajax_wrapper',
    ),
  );
  $b_default_load = FALSE;
  $param_title = '';
  $param_desc = '';
  if (strtolower($form_state['op']) == 'add') {
    $b_default_load = TRUE;
  }
  else {
    $val = $keyword->action;
    if ($val == 'url_redirect') {
      $param_title = t('Target');
      $param_desc = t('An internal or external URL to redirect');
    }
    elseif ($val == 'show_content_page') {
      $param_title = t('Content Title');
      $param_desc = t('Start typing the title of the target content.');
    }
    elseif ($val == 'show_product_page') {
      $param_title = t('Product Title');
      $param_desc = t('Start typing the title of the target product.');
    }
  }
  
  $retainValue = $keyword->parameter;
  if ((isset($form_state['values']['action'])) && ($form_state['values']['action'] != '')) {
    if ($form_state['values']['action'] != $keyword->action) {
      $retainValue = '';
    }
  }
 
  $form['parameter'] = array(
    '#type' => 'textfield',
    '#prefix' => "<div id='searchandise_form_ajax_wrapper'>",
    '#suffix' => "</div>",
    '#title' => $param_title,
    '#description' => $param_desc,
  );
  $form['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Online'),
    '#description' => t('Set status of Keywords'),
    '#default_value' => $keyword->status,
  );
  if ((isset($form_state['values']['action'])) && ($form_state['values']['action'] != '')) {
    if ($form_state['values']['action'] == 'url_redirect') {
      $form['parameter']['#title'] = t('Target');
      $form['parameter']['#description'] = t('An internal or external URL to redirect');
      $form['parameter']['#default_value'] = $retainValue;
    }
    elseif ($form_state['values']['action'] == 'show_content_page') {
      $form['parameter']['#title'] = t('Content Title');
      $form['parameter']['#description'] = t('Start typing the title of the target content.');
      $form['parameter']['#default_value'] = $retainValue;
      $form['parameter']['#autocomplete_path'] = 'searchandise/autocomplete_node_title';
    }
    elseif ($form_state['values']['action'] == 'show_product_page') {
      $form['parameter']['#title'] = t('Product Title');
      $form['parameter']['#description'] = t('Start typing the title of the target product.');
      $form['parameter']['#default_value'] = $retainValue;
      $form['parameter']['#autocomplete_path'] = 'searchandise/autocomplete_node_title';
    }
  }
  else {
    $form['parameter']['#default_value'] = $retainValue;
    if ($b_default_load) {
      $form['parameter']['#title'] = t('Target');
      $form['parameter']['#description'] = t('An internal or external URL to redirect');
      $form['action']['#default_value'] = 'options[1]';
    }
  }
  return $form;
}

function searchandise_ctools_export_ui_form_validate($form, &$form_state) {
  
  if (strtolower($form_state['op']) == 'add' && isset($form_state['values']['name'])) {
    $exists = db_query("select name from {searchandise_keywords} where name = '".$form_state['values']['name']."'")->fetchField();
    if ($exists) {
      form_set_error('name', t('A record with same name already exists.'));
    }
  }
  
  if (isset($form_state['values']['keywords'])) {
    $keywords = $form_state['values']['keywords'];
    $arrayOfKeywords = explode(",",$keywords);
    $result = array_map('trim', $arrayOfKeywords);
    $form_state['values']['keywords'] = implode(",", $result);   
  }
}

function searchandise_form_ajax_callback($form, $form_state) {
  return $form['parameter'];
}
