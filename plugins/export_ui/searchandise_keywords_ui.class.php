<?php

/**
 * @file
 * A custom Ctools Export UI class for Searchandise keywords.
 */

/**
 * Customizations of the Searchandise Keywords UI.
 */
class searchandise_keywords_ui extends ctools_export_ui {

  /**
   * Alter the keywords list form defined by the base class.
   */
  function list_form(&$form, &$form_state) {
    parent::list_form($form, $form_state);

    $help = t('Use this page to create and modify Search keywords which you can use to redirect to particular page.');
    

    $form['#prefix'] .= $help;
    
  }
  
  /**
   * Alter the form header defined by base class
   */
  function list_table_header() {
    $header = array();
    if (!empty($this->plugin['export']['admin_title'])) {
      $header[] = array('data' => t('Title'), 'class' => 'ctools-export-ui-title');
    }

    $header[] = array('data' => t('Name'), 'class' => 'ctools-export-ui-name');
    $header[] = array('data' => t('Storage'), 'class' => 'ctools-export-ui-storage');
    $header[] = array('data' => t('Keywords'), 'class' => '');
    $header[] = array('data' => t('Action'), 'class' => '');
    $header[] = array('data' => t('Target'), 'class' => '');
    $header[] = array('data' => t('Status'), 'class' => '');
    $header[] = array('data' => t('Operations'), 'class' => 'ctools-export-ui-operations');

    return $header;
  }
  
  /**
   * Alter the list to add more columns defined by base class
   */
  function list_build_row($item, &$form_state, $operations) {
  // Set up sorting
  $name = $item->{$this->plugin['export']['key']};
  $schema = ctools_export_get_schema($this->plugin['schema']);

  // Note: $item->{$schema['export']['export type string']} should have already been set up by export.inc so
  // we can use it safely.
  switch ($form_state['values']['order']) {
    case 'disabled':
      $this->sorts[$name] = empty($item->disabled) . $name;
      break;
    case 'title':
      $this->sorts[$name] = $item->{$this->plugin['export']['admin_title']};
      break;
    case 'name':
      $this->sorts[$name] = $name;
      break;
    case 'storage':
      $this->sorts[$name] = $item->{$schema['export']['export type string']} . $name;
      break;
  }

  $this->rows[$name]['data'] = array();
  $this->rows[$name]['class'] = !empty($item->disabled) ? array('ctools-export-ui-disabled') : array('ctools-export-ui-enabled');

  // If we have an admin title, make it the first row.
  if (!empty($this->plugin['export']['admin_title'])) {
    $this->rows[$name]['data'][] = array(
      'data' => check_plain($item->{$this->plugin['export']['admin_title']}),
      'class' => array('ctools-export-ui-title'),
    );
  }
  $this->rows[$name]['data'][] = array(
    'data' => check_plain($name),
    'class' => array('ctools-export-ui-name'),
  );
  $this->rows[$name]['data'][] = array(
    'data' => check_plain($item->{$schema['export']['export type string']}),
    'class' => array('ctools-export-ui-storage'),
  );
  $this->rows[$name]['data'][] = array(
    'data' => check_plain($item->keywords),
    'class' => array('')
  );
  $this->rows[$name]['data'][] = array(
    'data' => check_plain($item->action), 
    'class' => array('')
  );
  $this->rows[$name]['data'][] = array(
    'data' => check_plain($item->parameter), 
    'class' => array('')
  );
  $this->rows[$name]['data'][] = array(
    'data' => (check_plain($item->status) == 1) ? "Online" : "Offline",
    'class' => array('')
  );

  $ops = theme('links__ctools_dropbutton', array('links' => $operations, 'attributes' => array('class' => array('links', 'inline'))));

  $this->rows[$name]['data'][] = array(
    'data' => $ops,
    'class' => array('ctools-export-ui-operations'),
  );

  // Add an automatic mouseover of the description if one exists.
  if (!empty($this->plugin['export']['admin_description'])) {
    $this->rows[$name]['title'] = $item->{$this->plugin['export']['admin_description']};
  }
 }
 
 /**
   * Alter the list search fields to add more fields
   */
  function list_search_fields() {
    $fields = parent::list_search_fields();
    $fields[] = 'keywords';
    return $fields;
  }
}
