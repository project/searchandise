Searchandise
-----

The modules allows creating actions (like redirecting) that happen when a user
searches for a specific word in search blocks or pages that are provided by
Search API. Drupal's default search module is currently not supported.

Usage.
-----

1. Go to admin/config/search/searchandise .
2. Set the keywords that will trigger the action.
3. After saving, the selected action will run for anybody who searches for any
   of the listed keywords.


Actions
-----
Default actions that come with the module:

URL Redirect: Redirects the user to an external or internal page.
Show Content Page: Redirects the user to a node's page.
Show Product Page: Redirects the user to a product display node's page.

You can add your own actions implementing hook_searchandise_search_action() any
time!
