<?php
/**
 * @file
 *   Admin pages.
 */

/**
 * Autocomplete AJAX callback for selecting a node by node title.
 */
function searchandise_autocomplete_node_title_callback($string = "") {
  $matches = array();
  if ($string) {
    $query = db_select('node')
      ->fields('node', array('nid', 'title'))
      ->condition('title', db_like($string) . '%', 'LIKE');
      if (module_exists('commerce')) {
        $query->condition('type', searchandise_commerce_display_bundles(), 'NOT IN');
      }
      $result = $query->range(0, 10)
      ->execute();
    foreach ($result as $node) {
      $matches[$node->title . " [$node->nid]"] = check_plain($node->title);
    }
  }

  drupal_json_output($matches);
}

/**
 * Autocomplete AJAX callback for selecting a product by its title.
 */
function searchandise_autocomplete_product_title_callback($string = "") {
  $matches = array();
  if ($string) {
    $result = db_select('node')
      ->fields('node', array('nid', 'title'))
      ->condition('title', db_like($string) . '%', 'LIKE')
      ->condition('type', searchandise_commerce_display_bundles(), 'IN')
      ->range(0, 10)
      ->execute();
    foreach ($result as $product) {
      $matches[$product->title . " [$product->nid]"] = check_plain($product->title);
    }
  }

  drupal_json_output($matches);
}
